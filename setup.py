#!/usr/bin/env python

from setuptools import setup

__version__ = '0.0.1'

CLASSIFIERS = map(str.strip,
"""Environment :: Console
Environment :: X11 Applications :: QT
Natural Language :: English
Operating System :: POSIX :: Linux
Programming Language :: Python
Programming Language :: Python :: 3.6
""".splitlines())

entry_points = {
    'console_scripts': [
        'freedombox-desktop-client = freedombox_desktop_client.main:main',
    ]
}

setup(
    name="freedombox-desktop-client",
    version=__version__,
    author="Federico Ceratto",
    author_email="federico.ceratto@gmail.com",
    description="FreedomBox Desktop Client",
    license="GPLv3+",
    url="https://salsa.debian.org/freedombox-team/freedombox-desktop-client",
    long_description="",
    classifiers=CLASSIFIERS,
    keywords="desktop security",
    install_requires=[
        'setproctitle>=1.0.1',
    ],
    packages=['freedombox_desktop_client'],
    package_dir={'freedombox_desktop_client': 'freedombox_desktop_client'},
    platforms=['Linux'],
    zip_safe=False,
    entry_points=entry_points,
)
